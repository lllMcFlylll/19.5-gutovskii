// 19.5-gutovskii.cpp: определяет точку входа для консольного приложения.
//

#include "stdafx.h"
#include "iostream"
#include "string"

class Animal
{
public:
    virtual void Voice() const = 0
    {
        std::cout << "Animal say:";
    }
};

class Dog : public Animal
{
public:
    void Voice() const override
    {
        std::cout << "Woof!" << "\n";
    }
};

class Cat : public Animal
{
public:
    void Voice() const override
    {
        std::cout << "Meow!" << "\n";
    }
};

class Bird : public Animal
{
public:
    void Voice() const override
    {
        std::cout << "ChikChirik!" << "\n";
    }
};

int main()
{
    Animal* Animals[3];
    Animals[0] = new Dog();
    Animals[1] = new Cat();
    Animals[2] = new Bird();


    for (Animal* a : Animals)
        a->Voice();
		

	system("pause");
}


